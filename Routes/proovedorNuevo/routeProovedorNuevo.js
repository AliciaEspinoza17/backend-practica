const express = require('express');
const modelProovedor = require('../../Models/refaccionaria/modelProovedores');

let app = express();

app.post('/proovedor/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProovedor = new modelProovedor({
        nombreProovedor: body.nombreProovedor,
        direccionProovedor: body.direccionProovedor,
        telefonoProovedor: body.telefonoProovedor,
        correoProducto: body.correoProducto,
        productoProovedor: body.productoProovedor,
    });

    newSchemaProovedor
        .save()
        .then(
            (data) => {
                return res.status(200).json({
                    ok: true,
                    message: 'Datos Guardados',
                    data
                });
            }
        )
        .catch(
            (err) => {
                return res.status(500).json({
                    ok: false,
                    message: 'Error al ingresar los datos',
                    err
                });
            }
        )
});

app.get('/obtener/proovedor', async (req, res) => {
    const respuesta = await modelProovedor.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.delete('/borrar/proovedor/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelProovedor.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/proovedor/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelProovedor.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;