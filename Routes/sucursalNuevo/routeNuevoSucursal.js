const express = require('express');
const modelSucursal = require('../../Models/refaccionaria/modelSucursal');

let app = express();

app.post('/sucursal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaStock = new modelSucursal({
        nombreSucursal: body.nombreSucursal,
        direccionSucursal: body.direccionSucursal,
        telefonoSucursal: body.telefonoSucursal,
        correoSucursal: body.correoSucursal,
        encargadoSucursal: body.encargadoSucursal,
    });

    newSchemaStock
        .save()
        .then(
            (data) => {
                return res.status(200).json({
                    ok: true,
                    message: 'Datos Guardados',
                    data
                });
            }
        )
        .catch(
            (err) => {
                return res.status(500).json({
                    ok: false,
                    message: 'Error al ingresar los datos',
                    err
                });
            }
        )
});

app.get('/obtener/sucursal', async (req, res) => {
    const respuesta = await modelSucursal.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.delete('/borrar/sucursal/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelSucursal.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/sucursal/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelSucursal.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;