const express = require('express');
const modelEmpleado = require('../../Models/refaccionaria/modelEmpleado');

let app = express();

app.post('/empleado/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaEmpleado = new modelEmpleado({
        nombreEmpleado: body.nombreEmpleado,
        apellidoEmpleado: body.apellidoEmpleado,
        telefonoEmpleado: body.telefonoEmpleado,
        correoEmpleado: body.correoEmpleado,
        sueldoEmpleado: body.sueldoEmpleado,
    });

    newSchemaEmpleado
        .save()
        .then(
            (data) => {
                return res.status(200).json({
                    ok: true,
                    message: 'Datos Guardados',
                    data
                });
            }
        )
        .catch(
            (err) => {
                return res.status(500).json({
                    ok: false,
                    message: 'Error al ingresar los datos',
                    err
                });
            }
        )
});

app.get('/obtener/empleado', async (req, res) => {
    const respuesta = await modelEmpleado.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.delete('/borrar/empleado/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelEmpleado.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/empleado/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelEmpleado.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;