const express = require('express');
const app = express();



app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./empleadoNuevo/routeNuevoEmpleado'));
app.use(require('./proovedorNuevo/routeProovedorNuevo'));
app.use(require('./sucursalNuevo/routeNuevoSucursal'));
app.use(require('./stockNuevo/routeStockNuevo'));

module.exports = app;