const express = require('express');
const modelStock = require('../../Models/refaccionaria/modelStock');

let app = express();

app.post('/stock/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaStock = new modelStock({
        numeroStock: body.numeroStock,
        productoStock: body.productoStock,
        cantidadStock: body.cantidadStock,
        statusStock: body.statusStock,
        bloqueStock: body.bloqueStock,
    });

    newSchemaStock
        .save()
        .then(
            (data) => {
                return res.status(200).json({
                    ok: true,
                    message: 'Datos Guardados',
                    data
                });
            }
        )
        .catch(
            (err) => {
                return res.status(500).json({
                    ok: false,
                    message: 'Error al ingresar los datos',
                    err
                });
            }
        )
});

app.get('/obtener/stock', async (req, res) => {
    const respuesta = await modelStock.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.delete('/borrar/stock/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelStock.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/stock/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelStock.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;