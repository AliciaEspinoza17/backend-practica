const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoSucursal = new Schema({
    nombreSucursal: { type: String },
    direccionSucursal: { type: String },
    telefonoSucursal: { type: Number },
    correoSucursal: { type: String },
    encargadoSucursal: { type: String },
});

module.exports = mongoose.model('nuevoSucursal',nuevoSucursal);