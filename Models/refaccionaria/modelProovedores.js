const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoProovedor = new Schema({
    nombreProovedor: { type: String },
    direccionProovedor: { type: String },
    telefonoProovedor: { type: Number },
    correoProducto: { type: String },
    productoProovedor: { type: String },
});

module.exports = mongoose.model('nuevoProovedor',nuevoProovedor);