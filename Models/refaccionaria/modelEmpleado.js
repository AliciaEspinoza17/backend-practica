const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoEmpleado = new Schema({
    nombreEmpleado: { type: String },
    apellidoEmpleado: { type: String },
    telefonoEmpleado: { type: Number },
    correoEmpleado: { type: String },
    sueldoEmpleado: { type: Number },
});

module.exports = mongoose.model('nuevoEmpleado',nuevoEmpleado);