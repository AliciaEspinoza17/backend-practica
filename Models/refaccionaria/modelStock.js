const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoStock = new Schema({
    numeroStock: { type: Number },
    productoStock: { type: String },
    cantidadStock: { type: Number },
    statusStock: { type: String },
    bloqueStock: { type: String },
});

module.exports = mongoose.model('nuevoStock',nuevoStock);